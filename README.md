# Amazon Cognito JWT token Validation

Sample program to show how to validate idToken from Cognito logged user with node js;

## Procedures

Change file ```env_file``` into root dir in project.

```bash
REGION = AMAZON_REGION_HERE
USER_POOL_ID = COGNITO_USER_POOL_ID_HERE
# note put a valid and not expired key
TOKEN_TO_VALIDATE= PUT USER idToken TO TEST HERE
```
- REGION: Amazon aws region ```ex: us-east-1```
- USER_POOL_ID: Cognito User Pool id, you can get in General Settings in aws groups and users panel;
- TOKEN_TO_VALIDATE: User token to validate, try login in browser and get token from localStorage, it looks like: CognitoIdentityServiceProvider.15knn1c695o8uju3nnffql5b7s.{userNameHere}.idToken

## Usage

```node
#from cmd/term in root dir
npm install 
#if you use yarn
yarn install
#then run npm start
npm start
#using yarn
yarn start
```

## Project Structure
```bash
.
├── env_file
├── node_modules
│   ├── ... # node modules here supressed  
├── package.json
├── package-lock.json
├── README.md # this file
├── src
│   ├── boot.js # main program file
│   └── http.js # exported function to use in boot.js
├── yarn-error.log
└── yarn.lock

31 directories, 8 files
```

## Contributing
Pull requests are welcome.

##  References 2020/11/17
[Verifying Json web token cognito](https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-verifying-a-jwt.html)  
[Decode and verify Amazon cognito JWT tokens](https://github.com/awslabs/aws-support-tools/tree/master/Cognito/decode-verify-jwt)

## License
[MIT](https://choosealicense.com/licenses/mit/)