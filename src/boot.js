const  dotenv = require("dotenv").config();
const jwt = require("jsonwebtoken");
const jwkToPem = require("jwk-to-pem");
const { getCognitoJwtKeysDetails } = require("./http");



const getKeys = (keys) => {
    let jwtKeys = [];
    let jwtAlgs = [];
    let jwtKty = [];
    let jwtE = [];
    let jwtN = []; 
    let pem = undefined;
    let token = process.env.TOKEN_TO_VALIDATE;
    keys.map(key => {
        jwtKeys.push(key.kid);
        jwtAlgs.push(key.alg);
        jwtKty.push(key.kty);
        jwtE.push(key.e);
        jwtN.push(key.n);
    });

    // console.log('keys => ', jwtKeys);
    // console.log('algs => ', jwtAlgs);
    // console.log('keysType => ', jwtKty);

    const jwk = {
        alg: jwtAlgs[0],
        kid: jwtKeys[0],
        kty: jwtKty[0],
        e: jwtE[0],
        n: jwtN[0],
    };

    pem = jwkToPem(jwk); 
    jwt.verify(token,pem, { algorithms: jwtAlgs[0]}, (err, decoded) => {
        if (err) console.log(err);
        console.log(decoded);
    });
};

getCognitoJwtKeysDetails()
.then(({data}) => getKeys(data.keys))
.catch(e => console.log(e));


