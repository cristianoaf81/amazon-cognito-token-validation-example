const axios = require("axios");

const _REGION  = process.env.REGION;
const _USER_POOL_ID = process.env.USER_POOL_ID;

const BASE_URL=`https://cognito-idp.${_REGION}.amazonaws.com/${_USER_POOL_ID}/.well-known/jwks.json`;


const getCognitoJwtKeysDetails = async () => {
    
    try {
        return  await axios.get(BASE_URL);       
    } catch (error) {
        console.log(error);
        return undefined;
    }        
}

module.exports = {
    getCognitoJwtKeysDetails
}